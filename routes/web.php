<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('series/game-of-thrones/', ['middleware' => 'age:18', function () {
    return "eres mayor de edad y puedes ver este contenido";
}]);

*/
//Route::resource('users', 'UserController'); //recurso para usuarios, usercontroler

Route::post('users', 'UserController@store')->name('registrar');
Route::get('acceder', 'Seguridad\LoginController@index')->name('acceder');
Route::post('acceder', 'Seguridad\LoginController@login')->name('acceder_post');
Route::post('salir', 'Seguridad\LoginController@logout')->name('salir');
Route::get('/reservas','bookingsController@index');


Route::get('/', function(){
    return view('index');
});

Route::get('/home', function(){
    return view('index');
});

Route::get('/registrate',['middleware' => 'guest', function(){
    return view('register');
}]);

Route::get('/canchas', function(){
    return view('canchas');
});

Route::get('/misreservas', ['middleware' => 'auth', function(){
    return view('profile');
}]);

Route::get('/contactos', function(){
    return view('contactos');
});

Route::get('/partidos', ['middleware' => 'auth', function(){
    return view('bookings');
}]);

Route::get('/test', function(){
    return view('bookings');
});